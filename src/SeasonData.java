public class SeasonData{
    int year;
    int matchesNo;

    @Override
    public String toString() {
        return "[" +
                "year=" + year +
                ", No. of matches Played=" + matchesNo +
                ']';
    }

    public SeasonData(int year, int matchesNo) {
        this.year = year;
        this.matchesNo = matchesNo;
    }
}