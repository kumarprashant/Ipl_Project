public class Deliveries {
    public int id;
    public int inning;
    public String batting_team;
    public String bolling_team;
    public int over;
    public String bowler;
    public int is_super_over;
    public int extra_runs;
    public int total_runs;

    public Deliveries(String... args) {
        this.id = Integer.parseInt(args[0]);
        this.inning = Integer.parseInt(args[1]);
        this.batting_team = args[2];
        this.bolling_team = args[3];
        this.over = Integer.parseInt(args[4]);
        this.bowler= args[8];
        this.is_super_over = Integer.parseInt(args[9]);
        this.extra_runs = Integer.parseInt(args[16]);
        this.total_runs = Integer.parseInt(args[17]);
    }

    @Override
    public String toString() {
        return "Deliveries{" +
                "id=" + id +
                ", inning=" + inning +
                ", batting_team='" + batting_team + '\'' +
                ", bowling_team='" + bolling_team + '\'' +
                ", extra_runs=" + extra_runs +
                '}';
    }
}
