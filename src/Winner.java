public class Winner {
    int year;
    String team;
    int matches;

    public Winner(int year, String team, int matches) {
        this.year = year;
        this.team = team;
        this.matches = matches;
    }

    @Override
    public String toString() {
        return "[" +
                "Year=" + year +
                ", Team='" + team + '\'' +
                ", No of matches win=" + matches +
                ']';
    }
}
