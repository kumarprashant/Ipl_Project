public class Matches {
    public int id;
    public int season;
    public String city;
    public String date;
    public String team1;
    public String team2;
    public String toss_winner;
    public String toss_decision;
    public String result;
    public int dl_applied;
    public String winner;
    public int win_by_runs;
    public int win_by_wickets;
    public String player_of_match;
    public String venue;
//    public String umpire1;
//    public String umpire2;
//    public String umpire3;
    public Matches(String... args) {
        this.id = Integer.parseInt(args[0]);
        this.season = Integer.parseInt(args[1]);
        this.city = args[2];
        this.date = args[3];
        this.team1 =args[4];
        this.team2 = args[5];
        this.toss_winner = args[6];
        this.toss_decision = args[7];
        this.result = args[8];
        this.dl_applied = Integer.parseInt(args[9]);
        this.winner = args[10];
        this.win_by_runs = Integer.parseInt(args[11]);
        this.win_by_wickets = Integer.parseInt(args[12]);
        this.player_of_match = (args[13]);
        this.venue = args[14];
//        this.umpire1 = args[15];
//        this.umpire2 = args[16];
//        this.umpire3 = args[17];
    }

    @Override
    public String toString() {
        return "Matches{" +
                "id=" + id +
                ", season=" + season +
                ", city='" + city + '\'' +
                ", date=" + date +
                ", team1='" + team1 + '\'' +
                ", team2='" + team2 + '\'' +
                ", toss_winner='" + toss_winner + '\'' +
                ", toss_decision='" + toss_decision + '\'' +
                ", result='" + result + '\'' +
                ", dl_applied=" + dl_applied +
                ", winner='" + winner + '\'' +
                ", win_by_runs=" + win_by_runs +
                ", win_by_wickets=" + win_by_wickets +
                ", player_of_match='" + player_of_match + '\'' +
                ", venue='" + venue + '\'' +
//                ", umpire1='" + umpire1 + '\'' +
//                ", umpire2='" + umpire2 + '\'' +
//                ", umpire3='" + umpire3 + '\'' +
                '}';
    }
}
