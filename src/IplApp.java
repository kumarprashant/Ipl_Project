import java.io.BufferedReader;
import java.io.FileReader;
import java.util.*;

public class IplApp {
    Deliveries deliveries;
    static ArrayList<Matches> matchData = new ArrayList<>();
    static ArrayList<Deliveries> deliveriesData = new ArrayList<>();
    public static void main(String[] args){
        FileReader fileReader;
        String path="E:\\mountBlue\\ipl_Project\\ipl\\matches.csv";
        try {
            fileReader = new FileReader(path);
            BufferedReader bufferedReader=new BufferedReader(fileReader);
            bufferedReader.readLine();
            String s=bufferedReader.readLine();
            matchData= new ArrayList<>();
            while (s != null){
                String[] str=s.split(",");
                matchData.add(new Matches(str));
                s=bufferedReader.readLine();
            }
            bufferedReader.close();
        }catch (Exception e){
            System.out.println("File Not Found at the given location");
        }
//---------------------------------------------------------------------------------------------
        path="E:\\mountBlue\\ipl_Project\\ipl\\deliveries.csv";
        try {
            fileReader = new FileReader(path);
            BufferedReader bufferedReader=new BufferedReader(fileReader);
            bufferedReader.readLine();
            String s=bufferedReader.readLine();
            deliveriesData= new ArrayList<>();
            while (s != null){
                String[] str=s.split(",");
                deliveriesData.add(new Deliveries(str));
                s=bufferedReader.readLine();
            }
            bufferedReader.close();
        }catch (Exception e){
            System.out.println("File Not Found at the given location");
        }
        findAnswer();
    }
    static void findAnswer(){
        Scanner scn=new Scanner(System.in);
        System.out.println("Please enter the number acoording to question to get the answer");
        System.out.println("1. Number of matches played per year of all the years in IPL");
        System.out.println("2. Number of matches won of all teams over all the years of IPL");
        System.out.println("3. For the year 2016 get the extra runs conceded per team");
        System.out.println("4. For the year 2015 get the top economical bowlers");
        int val=scn.nextInt();
        scn.close();
        switch (val) {
            case 1:
                getNumberOfMatchesPlayed();
                break;
            case 2:
                getMatchesWonOfAllTeam();
                break;
            case 3:
                getTheExtraRunConceded();
                break;
            case 4:
                getTheTopEconomicalBowlers();
                break;
            default:
                System.out.println("Please enter a valid selection");
                findAnswer();
        }
    }
    static void getNumberOfMatchesPlayed(){

        int year = 0;
        int count=0;
        ArrayList seasonMatch = new ArrayList();
        for(int i=0; i<matchData.size(); i++){
            if(year != (int)matchData.get(i).season){
                if(year != 0 && count!=0)
                    seasonMatch.add(new SeasonData(year, count));
                year=(int)matchData.get(i).season;
                count = 0;
                count++;
            }
            else if(year == (int)matchData.get(i).season){
                count++;
            }
        }
        for(int i=0; i<seasonMatch.size(); i++){
            System.out.println(seasonMatch.get(i));
        }

    }
    static  void getMatchesWonOfAllTeam(){
        int year = 0;
        int count=0;
        ArrayList seasonMatch = new ArrayList();
        ArrayList<String> winner=new ArrayList<>();
        continueAdd:
        for(int i=0; i<matchData.size(); i++){
            if(year != (int)matchData.get(i).season || matchData.size()-1 == i){
                if(year != 0) {
                    Object[] obj=winner.toArray();
                    Arrays.sort(obj);
                    String winnerName=(String) obj[0];
                    for(int j=0; j< obj.length; j++){
                        if(winnerName.equals((String)obj[j]) && obj.length-1 != j) {
                            count++;
                        }else{
                            seasonMatch.add(new Winner(year,winnerName,count));
                            winnerName=(String)obj[j];
                            count=0;
                            count++;
                        }

                    }
                }
                year=(int)matchData.get(i).season;
                winner = new ArrayList<>();
                winner.add(matchData.get(i).winner);
            }
            else if(year == (int)matchData.get(i).season){
                winner.add(matchData.get(i).winner);
            }
        }
        for(int i=0; i<seasonMatch.size(); i++){
            System.out.println(seasonMatch.get(i));
        }
    }
    static void getTheExtraRunConceded(){
        ArrayList<Integer> data16=new ArrayList<>();
        ArrayList<Deliveries> extraRun = new ArrayList<>();
        for(int i=0; i<matchData.size(); i++) {
            if ((int) matchData.get(i).season == 2016) {
                data16.add(matchData.get(i).id);
            }
        }
        int j=0;
        int temp = 0;
        for(int i=0; i<deliveriesData.size(); i++){
            if(j<data16.size()){
                if(deliveriesData.get(i).id == data16.get(j)){
                    extraRun.add(deliveriesData.get(i));
                    temp=1;
                }else if(temp == 1){
                    j++;
                    i--;
                }
            }
        }
        int value= 0;
        Map<String, Integer> extraRunOutput = new HashMap<>();
        for(int i=0;  i<extraRun.size(); i++){
            if(extraRunOutput.containsKey(extraRun.get(i).bolling_team)){
                value=extraRun.get(i).extra_runs + extraRunOutput.get(extraRun.get(i).bolling_team);
            }else{
                value=extraRun.get(i).extra_runs;
            }
            extraRunOutput.put(extraRun.get(i).bolling_team,value);
        }
        System.out.println(extraRunOutput);
    }
    static void getTheTopEconomicalBowlers() {
        ArrayList<Integer> data16 = new ArrayList<>();
        ArrayList<Deliveries> extraRun = new ArrayList<>();
        for (int i = 0; i < matchData.size(); i++) {
            if ((int) matchData.get(i).season == 2015) {
                data16.add(matchData.get(i).id);
            }
        }
        int j = 0;
        int temp = 0;
        for (int i = 0; i < deliveriesData.size(); i++) {
            if (j < data16.size()) {
                if (deliveriesData.get(i).id == data16.get(j)) {
                    extraRun.add(deliveriesData.get(i));
                    temp = 1;
                } else if (temp == 1) {
                    j++;
                    i--;
                }
            }
        }
        String bowler=null;
        int overCount= 0;
        int runCount= 0;
        Map<String, Integer> over= new HashMap<>();
        Map<String, Integer> run=new HashMap<>();
        for(int i=0; i<extraRun.size(); i++){
            if(extraRun.get(i).is_super_over == 0){
                if((extraRun.get(i).bowler).equals(bowler)){
                    runCount+=extraRun.get(i).total_runs;
                }
                else{
                    if(bowler != null){
                        if(over.containsKey(extraRun.get(i).bowler)){
                            runCount=run.get(extraRun.get(i).bowler)+runCount;
                            overCount=over.get(extraRun.get(i).bowler)+1;
                        }else
                            overCount=1;
                        over.put(extraRun.get(i).bowler,overCount);
                        run.put(extraRun.get(i).bowler,runCount);
                        runCount = 0;
                        bowler = extraRun.get(i).bowler;
                    }else
                        bowler = extraRun.get(i).bowler;
                }
            }

        }
        System.out.println(over);
        System.out.println(run);
    }
//    static void getFileLoading(String path){
//        FileReader fileReader;
//        try {
//            fileReader = new FileReader(path);
//            BufferedReader bufferedReader=new BufferedReader(fileReader);
//            bufferedReader.readLine();
//            String s=bufferedReader.readLine();
//            matchData= new ArrayList<>();
//            while (s != null){
//                String[] str=s.split(",");
//                matchData.add(new Matches(str));
//                s=bufferedReader.readLine();
//            }
//            bufferedReader.close();
//        }catch (Exception e){
//            System.out.println("File Not Found at the given location");
//        }
//    }
}


